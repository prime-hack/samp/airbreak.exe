#ifndef AIRBREAK_H
#define AIRBREAK_H

#include "ui_airbreak.h"
#include "otherprocess.h"
#include "selectprocessname.h"
#include <QSettings>
#include <QTimer>
#include <QMap>

const uint pLocalPlayer = 0xB6F5F0;
const uint pCamRotation = 0xB6F258;

struct RwV3D{
	float fX;
	float fY;
	float fZ;
};

struct RwMatrix{
	RwV3D	right;
	DWORD	flags;
	RwV3D	up;
	float	pad_u;
	RwV3D	at; // front
	float	pad_a;
	RwV3D	pos;
	float	pad_p;

	RwV3D getOffset(RwV3D offset);
};

struct CPlaceable
{
	uint		vtbl;   // Указатель на виртуальную таблицу методов для этого объекта
	RwV3D		pos;	// Позиция RwV3D
	float		angle;   // Угол по оси Z
	uint		pMatrix; // Указатель на матрицу позиции и вращения
};

struct CEntity
{
	CPlaceable		placeable;
	DWORD			ptr_RwObject;
	/********** BEGIN CFLAGS **************/
	unsigned long	bUsesCollision : 1;			// does entity use collision
	unsigned long	bCollisionProcessed : 1;	// has object been processed by a ProcessEntityCollision function
	unsigned long	bIsStatic : 1;			// is entity static
	unsigned long	bHasContacted : 1;		// has entity processed some contact forces
	unsigned long	bIsStuck : 1;			// is entity stuck
	unsigned long	bIsInSafePosition : 1;	// is entity in a collision free safe position
	unsigned long	bWasPostponed : 1;		// was entity control processing postponed
	unsigned long	bIsVisible : 1;			//is the entity visible
	unsigned long	bIsBIGBuilding : 1;		// Set if this entity is a big building
	unsigned long	bRenderDamaged : 1;		// use damaged LOD models for objects with applicable damage
	unsigned long	bStreamingDontDelete : 1;	// Dont let the streaming remove this
	unsigned long	bRemoveFromWorld : 1;		// remove this entity next time it should be processed
	unsigned long	bHasHitWall : 1;			// has collided with a building (changes subsequent collisions)
	unsigned long	bImBeingRendered : 1;		// don't delete me because I'm being rendered
	unsigned long	bDrawLast : 1;			// draw object last
	unsigned long	bDistanceFade : 1;		// Fade entity because it is far away
	unsigned long	bDontCastShadowsOn : 1; // Dont cast shadows on this object
	unsigned long	bOffscreen : 1;			// offscreen flag. This can only be trusted when it is set to true
	unsigned long	bIsStaticWaitingForCollision : 1;	// this is used by script created entities - they are static until the collision is loaded below them
	unsigned long	bDontStream : 1;			// tell the streaming not to stream me
	unsigned long	bUnderwater : 1;			// this object is underwater change drawing order
	unsigned long	bHasPreRenderEffects : 1;	// Object has a prerender effects attached to it
	unsigned long	bIsTempBuilding : 1;		// whether or not the building is temporary (i.e. can be created and deleted more than once)
	unsigned long	bDontUpdateHierarchy : 1;	// Don't update the aniamtion hierarchy this frame
	unsigned long	bHasRoadsignText : 1;		// entity is roadsign and has some 2deffect text stuff to be rendered
	unsigned long	bDisplayedSuperLowLOD : 1;
	unsigned long	bIsProcObject : 1;			// set object has been generate by procedural object generator
	unsigned long	bBackfaceCulled : 1;		// has backface culling on
	unsigned long	bLightObject : 1;			// light object with directional lights
	unsigned long	bUnimportantStream : 1;		// set that this object is unimportant, if streaming is having problems
	unsigned long	bTunnel : 1;			// Is this model part of a tunnel
	unsigned long	bTunnelTransition : 1;	// This model should be rendered from within and outside of the tunnel
	/********** END CFLAGS **************/

	uint8_t			wSeedColFlags;			// 0x20
	uint8_t			wSeedVisibleFlags;		// 0x21
	WORD			modelID;
	uint8_t			__unknown_36[4];		// 36 - collision related (objects only?)

	//CReferences *pReferences; //36
	uint32_t		*m_pLastRenderedLink;	// 40 CLink<CEntity*>* m_pLastRenderedLink;
	uint16_t		timer;			// 44
	uint8_t			m_iplIndex;		// 46 used to define which IPL file object is in
	uint8_t			interior_id;	// 47
	uint8_t			__unknown_48[6];		// 48

	//********* BEGIN CEntityInfo **********//
	uint8_t			nType : 3;				// 54 what type is the entity (2 == Vehicle)
	uint8_t			nStatus : 5;			// 54 control status

	// 55 alignment
	//********* END CEntityInfo **********//
	uint8_t			__unknown_56[8];		// 56
	uint8_t			quantumPhysics;			// 64 // this really needs to be figured out cos it's probably a bit field
};

struct stByteBits{
	union{
		uint8_t value;
		struct{
			uint8_t bit1 : 1;
			uint8_t	bit2 : 1;
			uint8_t bit3 : 1;
			uint8_t bit4 : 1;
			uint8_t bit5 : 1;
			uint8_t	bit6 : 1;
			uint8_t bit7 : 1;
			uint8_t bit8 : 1;
		};
	};
};

struct CPhysical
{
	CEntity entity;
	union {
		struct
		{
			union{
				struct{
					uint8_t __unk1_nImmunities : 1;
					uint8_t       bUsesGravity : 1;
					uint8_t __unk3_nImmunities : 1; // used for lock in 0519:
					uint8_t __unk4_nImmunities : 1; // used for lock in 0519:
					uint8_t __unk5_nImmunities : 1;
					uint8_t __unk6_nImmunities : 1;
					uint8_t __unk7_nImmunities : 1;
					uint8_t __unk8_nImmunities : 1;
				};
				uint8_t			nImmunities;			// 65
			};
			union{
				struct{
					uint8_t __unk1__unknown_66 : 1;
					uint8_t			bIsOnPlace : 1;
					uint8_t __unk3__unknown_66 : 1;
					uint8_t __unk4__unknown_66 : 1;
					uint8_t __unk5__unknown_66 : 1;
					uint8_t				 bLock : 1;
					uint8_t __unk7__unknown_66 : 1;
					uint8_t __unk8__unknown_66 : 1;
				};
				uint8_t		   __unknown_66;			// 66
			};
			union{
				struct
				{
					uint8_t            soft : 1;
					uint8_t       pedFreeze : 1;
					uint8_t    bullet_proof : 1;
					uint8_t      fire_proof : 1;
					uint8_t collision_proof : 1;
					uint8_t     melee_proof : 1;
					uint8_t     __unk7_flag : 1;
					uint8_t explosion_proof : 1;
				};
				uint8_t			  flags;			/* 67 immunities */
			};
			union{
				struct{
					uint8_t __unk1__unknown_68 : 1;
					uint8_t __unk2__unknown_68 : 1;
					uint8_t __unk3__unknown_68 : 1;
					uint8_t __unk4__unknown_68 : 1;
					uint8_t __unk5__unknown_68 : 1;
					uint8_t __unk6__unknown_68 : 1;
					uint8_t __unk7__unknown_68 : 1;
					uint8_t __unk8__unknown_68 : 1;
				};
				uint8_t		   __unknown_68;			/* 68 */
			};
		};
		uint32_t			immunities;			// 65
	};
	RwV3D				  speed;
	RwV3D				   spin;
	RwV3D		   speed_rammed;
	RwV3D			spin_rammed;
	uint8_t	  __unknown_116[24];			/* 116 */
	float				   mass;
	float		massWhenTurning;
	float			gripDivider;
	// 1.0 = 1 x gGrip
	// 10.1 = 10 x gGrip
	// 100.0 = g / 100Grip
	float			 massToGrip;
	float			 Elasticity;
	float			   Buoyancy;
	RwV3D			 massCenter;
	void		 *__unknown_176;			/* 176 - pointer to a "entry node info" pool item */
	void		 *__unknown_180;			/* 180 - pointer to a "ptr node Double" pool item */
	byte		  __unknown_184;
	WORD		 collisionCount;
	byte		  __unknown_187;
	CPhysical  *last_collided_object;
	CPhysical  *last_collided_object2;
	CPhysical  *last_collided_object3;
	CPhysical  *last_collided_object4;
	CPhysical  *last_collided_object5;
	CPhysical  *last_collided_object6;
	float				 speed_z;			/* 212 */
	float			 damageValue;			/* 216 мб это таймер коллизии */
	CPhysical		  *damagedBy;			/* 220 он может быть CPed'ом */
	RwV3D	  collided_pos_local;
	RwV3D			collided_pos;
	WORD		   collided_part;
	// ѕримечание: ƒл€ автомобилей используютс€ следующие номера частей :
	// 		1 - капот ? (лева€ передн€€ фара ? )
	// 		2 - багажник ?
	// 		3 - передний бампер ?
	// 		4 - задний бампер ?
	// 		5 - лева€ передн€€ дверь
	// 		6 - права€ передн€€ дверь
	// 		7 - лева€ задн€€ дверь
	// 		8 - права€ задн€€ дверь
	// 		9 - левое крыло ?
	// 		10 - правое крыло ?
	// 		17 - права€ передн€€ фара ?
	CPhysical   *attachedSubject;
	RwV3D			  target_pos;
	RwV3D			target_angle;
	byte	   __unknown_280[16];
	CPhysical   *collide_ignored;
	float			  lightLevel;
	DWORD		   __unknown_304;
	DWORD			*CShadowData;
};

class AirBreak : public QMainWindow, private Ui::AirBreak
{
	Q_OBJECT

public:
	explicit AirBreak(QWidget *parent = 0);
	~AirBreak();

	virtual void WindowsEvents(UINT uMsg, int keyId);

protected:
	void changeEvent(QEvent *e);

private slots:
	void on_actionExit_triggered();

	void on_actionAttach_triggered();

	void on_toggle_textChanged(const QString &arg1);

	void on_actionDetach_triggered();

	void on_up_textChanged(const QString &arg1);

	void on_down_textChanged(const QString &arg1);

	void on_forward_textChanged(const QString &arg1);

	void on_backward_textChanged(const QString &arg1);

	void on_left_textChanged(const QString &arg1);

	void on_right_textChanged(const QString &arg1);

public slots:
	void setProcessName(QString procName);
	void update();

private:
	SelectProcessName *selector;
	OtherProcess *proc;
	QSettings *settings;
	QTimer *timer;
	bool status;

	int key_toggle = VK_RSHIFT;
	int key_up = VK_SPACE;
	int key_down = VK_LSHIFT;
	int key_forward = 'W';
	int key_backward = 'S';
	int key_left = 'A';
	int key_right = 'D';

	QMap<int, bool> key;
};

#endif // AIRBREAK_H
