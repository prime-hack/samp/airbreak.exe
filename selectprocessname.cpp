#include "selectprocessname.h"

SelectProcessName::SelectProcessName(QWidget *parent) :
	QDialog(parent)
{
	setupUi(this);
}

void SelectProcessName::changeEvent(QEvent *e)
{
	QDialog::changeEvent(e);
	switch (e->type()) {
		case QEvent::LanguageChange:
			retranslateUi(this);
			break;
		default:
			break;
	}
}

void SelectProcessName::on_buttonBox_accepted()
{
	emit onAccepted(proc->text());
}
