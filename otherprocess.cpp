#include "otherprocess.h"

OtherProcess::OtherProcess(QString procName)
{
	setProcess(procName);
}

OtherProcess::~OtherProcess()
{
	CloseHandle(proc);
}

void OtherProcess::setProcess(QString procName)
{
	if (proc != nullptr)
		CloseHandle(proc);

	this->procName = procName;
	this->pid = getPid();
	proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
}

void *OtherProcess::alloc(size_t size)
{
	return VirtualAllocEx(proc, NULL, size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
}

void OtherProcess::free(void *address, size_t size)
{
	VirtualFreeEx(proc, address, size, MEM_RELEASE);
}

void OtherProcess::protect(void *address, size_t size, DWORD newProtect, DWORD *oldProtect)
{
	VirtualProtectEx(proc, address, size, newProtect, oldProtect);
}

void OtherProcess::memset(void *address, char character, size_t size)
{
	DWORD dwProtect;
	protect(address, size, PAGE_EXECUTE_READWRITE, &dwProtect);
	for(int i = 0; i < size; ++i)
		WriteProcessMemory( proc, (void*)((uint)address + i), &character, 1, NULL );
	protect(address, size, dwProtect, NULL);
}

void OtherProcess::memcpy(void *address, char *buffer, size_t size)
{
	DWORD dwProtect;
	protect(address, size, PAGE_EXECUTE_READWRITE, &dwProtect);
	for(int i = 0; i < size; ++i)
		WriteProcessMemory( proc, (void*)((uint)address + i), &(buffer[i]), 1, NULL );
	protect(address, size, dwProtect, NULL);
}

DWORD OtherProcess::loadModules()
{
	HANDLE h;
	DWORD dll = 0;

	MODULEENTRY32 me32;

	me32.dwSize = sizeof(MODULEENTRY32);

	h = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, this->pid);

	if (h == INVALID_HANDLE_VALUE) {
		::CloseHandle(h);
		return dll;
	}

	if (!::Module32First(h, &me32)) {
		::CloseHandle(h);
		return dll;
	}

	do {
		modules[QString::fromWCharArray(me32.szModule)] = (DWORD)me32.modBaseAddr;
	} while (::Module32Next(h, &me32));

	::CloseHandle(h);

	return dll;
}

int OtherProcess::getPid()
{

	HANDLE pHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 ProcessEntry;
	DWORD pid;
	ProcessEntry.dwSize = sizeof(ProcessEntry);
	bool Loop = Process32First(pHandle, &ProcessEntry);

	while (Loop)
	{
		if (ProcessEntry.szExeFile == procName.toStdWString())
		{
			pid = ProcessEntry.th32ProcessID;
			if (GetCurrentProcessId() != pid)
			{
				CloseHandle(pHandle);
				return pid;
			}
		}
		Loop = Process32Next(pHandle, &ProcessEntry);
	}
	return 0;
}
