#include "airbreak.h"
#include <QRegExp>
#include <cmath>

static AirBreak *airBreak = nullptr;
HHOOK hHook;

struct key_alias
{
	int		key;
	char	*name;
};
static struct key_alias key_alias[] =
{
{ VK_LBUTTON, "VK_LBUTTON" },
{ VK_RBUTTON, "VK_RBUTTON" },
{ VK_MBUTTON, "VK_MBUTTON" },
{ VK_XBUTTON1, "VK_XBUTTON1" },
{ VK_XBUTTON2, "VK_XBUTTON2" },
{ VK_BACK, "VK_BACK" },
{ VK_TAB, "VK_TAB" },
{ VK_CAPITAL, "VK_CAPITAL" },
{ VK_RETURN, "VK_RETURN" },
{ VK_SHIFT, "VK_SHIFT" },
{ VK_LSHIFT, "VK_LSHIFT" },
{ VK_RSHIFT, "VK_RSHIFT" },
{ VK_CONTROL, "VK_CONTROL" },
{ VK_LCONTROL, "VK_LCONTROL" },
{ VK_RCONTROL, "VK_RCONTROL" },
{ VK_MENU, "VK_MENU" },
{ VK_LMENU, "VK_LMENU" },
{ VK_RMENU, "VK_RMENU" },
{ VK_SPACE, "VK_SPACE" },
{ VK_PRIOR, "VK_PRIOR" },
{ VK_NEXT, "VK_NEXT" },
{ VK_END, "VK_END" },
{ VK_HOME, "VK_HOME" },
{ VK_LEFT, "VK_LEFT" },
{ VK_UP, "VK_UP" },
{ VK_RIGHT, "VK_RIGHT" },
{ VK_DOWN, "VK_DOWN" },
{ VK_INSERT, "VK_INSERT" },
{ VK_DELETE, "VK_DELETE" },
{ VK_PAUSE, "VK_PAUSE" },
{ VK_NUMPAD0, "VK_NUMPAD0" },
{ VK_NUMPAD1, "VK_NUMPAD1" },
{ VK_NUMPAD2, "VK_NUMPAD2" },
{ VK_NUMPAD3, "VK_NUMPAD3" },
{ VK_NUMPAD4, "VK_NUMPAD4" },
{ VK_NUMPAD5, "VK_NUMPAD5" },
{ VK_NUMPAD6, "VK_NUMPAD6" },
{ VK_NUMPAD7, "VK_NUMPAD7" },
{ VK_NUMPAD8, "VK_NUMPAD8" },
{ VK_NUMPAD9, "VK_NUMPAD9" },
{ VK_MULTIPLY, "VK_MULTIPLY" },
{ VK_ADD, "VK_ADD" },
{ VK_SEPARATOR, "VK_SEPARATOR" },
{ VK_SUBTRACT, "VK_SUBTRACT" },
{ VK_DECIMAL, "VK_DECIMAL" },
{ VK_DIVIDE, "VK_DIVIDE" },
{ VK_F1, "VK_F1" },
{ VK_F2, "VK_F2" },
{ VK_F3, "VK_F3" },
{ VK_F4, "VK_F4" },
{ VK_F5, "VK_F5" },
{ VK_F6, "VK_F6" },
{ VK_F7, "VK_F7" },
{ VK_F8, "VK_F8" },
{ VK_F9, "VK_F9" },
{ VK_F10, "VK_F10" },
{ VK_F11, "VK_F11" },
{ VK_F12, "VK_F12" },
{ VK_F13, "VK_F13" },
{ VK_F14, "VK_F14" },
{ VK_F15, "VK_F15" },
{ VK_F16, "VK_F16" },
{ VK_F17, "VK_F17" },
{ VK_F18, "VK_F18" },
{ VK_F19, "VK_F19" },
{ VK_F20, "VK_F19" },
{ VK_F21, "VK_F21" },
{ VK_F22, "VK_F22" },
{ VK_F23, "VK_F23" },
{ VK_F24, "VK_F24" },
{ VK_OEM_PLUS, "VK_OEM_PLUS" },
{ VK_OEM_COMMA, "VK_OEM_COMMA" },
{ VK_OEM_MINUS, "VK_OEM_MINUS" },
{ VK_OEM_PERIOD, "VK_OEM_PERIOD" },
{ VK_OEM_1, "VK_OEM_1" },
{ VK_OEM_2, "VK_OEM_2" },
{ VK_OEM_3, "VK_OEM_3" },
{ VK_OEM_4, "VK_OEM_4" },
{ VK_OEM_5, "VK_OEM_5" },
{ VK_OEM_6, "VK_OEM_6" },
{ VK_OEM_7, "VK_OEM_7" },
{ VK_OEM_8, "VK_OEM_8" },
{ VK_BROWSER_BACK, "VK_BROWSER_BACK" },
{ VK_BROWSER_FORWARD, "VK_BROWSER_FORWARD" },
{ VK_BROWSER_REFRESH, "VK_BROWSER_REFRESH" },
{ VK_BROWSER_STOP, "VK_BROWSER_STOP" },
{ VK_BROWSER_SEARCH, "VK_BROWSER_SEARCH" },
{ VK_BROWSER_FAVORITES, "VK_BROWSER_FAVORITES" },
{ VK_BROWSER_HOME, "VK_BROWSER_HOME" },
{ VK_VOLUME_MUTE, "VK_VOLUME_MUTE" },
{ VK_VOLUME_UP, "VK_VOLUME_UP" },
{ VK_VOLUME_DOWN, "VK_VOLUME_DOWN" },
{ VK_MEDIA_NEXT_TRACK, "VK_MEDIA_NEXT_TRACK" },
{ VK_MEDIA_PREV_TRACK, "VK_MEDIA_PREV_TRACK" },
{ VK_MEDIA_STOP, "VK_MEDIA_STOP" },
{ VK_MEDIA_PLAY_PAUSE, "VK_MEDIA_PLAY_PAUSE" },
{ VK_LAUNCH_MAIL, "VK_LAUNCH_MAIL" },
{ VK_LAUNCH_MEDIA_SELECT, "VK_MEDIA_PREV_TRACK" },
{ VK_LAUNCH_APP1, "VK_LAUNCH_APP1" },
{ VK_LAUNCH_APP1, "VK_LAUNCH_APP1" },
{ 0, "&0" },
};

int getKey(QString key)
{
	QRegExp re(R"(<\d+>)");
	key = key.toUpper();
	if (key.length() == 1){
		char ch = key[0].toLatin1();
		if (ch >= 'A' && ch <= 'Z')
			return (int)ch;
		if (ch >= '0' && ch <= '9')
			return (int)ch;
	}
	for (int i = 0; key_alias[i].key != 0; ++i)
		if (key == key_alias[i].name)
			return key_alias[i].key;
	if (re.indexIn(key) == 0)
		return key.toInt();
	return 0;
}

LRESULT CALLBACK MyLowLevelKeyBoardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (wParam == WM_KEYDOWN || wParam == WM_KEYUP ||
		wParam == WM_SYSKEYDOWN || wParam == WM_SYSKEYUP){
		if (wParam == WM_SYSKEYDOWN)
			wParam = WM_KEYDOWN;
		else if (wParam == WM_SYSKEYUP)
			wParam = WM_KEYUP;
		KBDLLHOOKSTRUCT* key = (KBDLLHOOKSTRUCT*)lParam;
		if (airBreak != nullptr)
			airBreak->WindowsEvents(wParam, MapVirtualKey(key->scanCode,3));
	}
	return CallNextHookEx(hHook,nCode,wParam,lParam);
}

AirBreak::AirBreak(QWidget *parent) :
	QMainWindow(parent)
{
	setupUi(this);
	settings = new QSettings("Prime-Hack", "AirBreak", this);
	toggle->setText(settings->value("toggle", "VK_RSHIFT").toString());
	key_toggle = getKey(toggle->text());
	up->setText(settings->value("up", "VK_SPACE").toString());
	key_up = getKey(up->text());
	down->setText(settings->value("down", "VK_LSHIFT").toString());
	key_down = getKey(down->text());
	forward->setText(settings->value("forward", "W").toString());
	key_forward = getKey(forward->text());
	backward->setText(settings->value("backward", "S").toString());
	key_backward = getKey(backward->text());
	left->setText(settings->value("left", "A").toString());
	key_left = getKey(left->text());
	right->setText(settings->value("right", "D").toString());
	key_right = getKey(right->text());
	speed->setValue(settings->value("speed", 0.1).toFloat());
	rotate->setChecked(settings->value("rotate", true).toBool());
	selector = new SelectProcessName(this);
	proc = new OtherProcess();
	timer = new QTimer(this);
	airBreak = this;
	status = false;

	QObject::connect(selector, &SelectProcessName::onAccepted, this, &AirBreak::setProcessName);
	QObject::connect(timer, &QTimer::timeout, this, &AirBreak::update);

	hHook = SetWindowsHookEx(WH_KEYBOARD_LL,MyLowLevelKeyBoardProc,GetModuleHandle( NULL ),0);
	timer->start(0);
}

AirBreak::~AirBreak()
{
	if (valid_toggle->isChecked())
		settings->setValue("toggle", toggle->text());
	if (valid_up->isChecked())
		settings->setValue("up", up->text());
	if (valid_down->isChecked())
		settings->setValue("down", down->text());
	if (valid_forward->isChecked())
		settings->setValue("forward", forward->text());
	if (valid_backward->isChecked())
		settings->setValue("backward", backward->text());
	if (valid_left->isChecked())
		settings->setValue("left", left->text());
	if (valid_right->isChecked())
		settings->setValue("right", right->text());
	settings->setValue("speed", speed->value());
	settings->setValue("rotate", rotate->isChecked());
	airBreak = nullptr;
	delete proc;
}

void AirBreak::WindowsEvents(UINT uMsg, int keyId)
{
	if (uMsg == WM_KEYDOWN){
		key[keyId] = true;
		if (keyId == key_toggle){
			uint pPed = proc->read<uint>((void*)pLocalPlayer);
			if (pPed == 0)
				return;

			uint ped_status = proc->read<uint>((void*)(pPed + 0x530));
			if (ped_status == 54 || ped_status == 55 || ped_status == 63)
				return;
			status ^= true;

			uint local = pPed;
			if (ped_status == 50)
				local = proc->read<uint>((void*)(pPed + 0x58C));

			stByteBits __unknown_66;
			__unknown_66.value = proc->read<uint8_t>((void*)(local + offsetof(CPhysical, __unknown_66)));
			__unknown_66.bit6 = status; // bLock
			proc->write<uint8_t>((void*)(local + offsetof(CPhysical, __unknown_66)), __unknown_66.value);

			stByteBits flags;
			flags.value = proc->read<uint8_t>((void*)(local + sizeof(CPlaceable) + 8));
			flags.bit1 = status ^ true; // soft
			proc->write<uint8_t>((void*)(local + sizeof(CPlaceable) + 8), flags.value);
		}
	}
	if (uMsg == WM_KEYUP)
		key[keyId] = false;
}

void AirBreak::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
		case QEvent::LanguageChange:
			retranslateUi(this);
			break;
		default:
			break;
	}
}

void AirBreak::on_actionExit_triggered()
{
	close();
}

void AirBreak::on_actionAttach_triggered()
{
	selector->show();
}

void AirBreak::setProcessName(QString procName)
{
	proc->setProcess(procName);
}

void AirBreak::update()
{
	if (proc == nullptr)
		return;
	if (!proc->hasSuccess()){
		rotate->setEnabled(false);
		toggle->setEnabled(false);
		up->setEnabled(false);
		down->setEnabled(false);
		forward->setEnabled(false);
		backward->setEnabled(false);
		left->setEnabled(false);
		right->setEnabled(false);
		speed->setEnabled(false);
		label_toggle->setEnabled(false);
		label_up->setEnabled(false);
		label_down->setEnabled(false);
		label_forward->setEnabled(false);
		label_backward->setEnabled(false);
		label_left->setEnabled(false);
		label_right->setEnabled(false);
		label_speed->setEnabled(false);
		return;
	} else {
		rotate->setEnabled(true);
		toggle->setEnabled(true);
		up->setEnabled(true);
		down->setEnabled(true);
		forward->setEnabled(true);
		backward->setEnabled(true);
		left->setEnabled(true);
		right->setEnabled(true);
		speed->setEnabled(true);
		label_toggle->setEnabled(true);
		label_up->setEnabled(true);
		label_down->setEnabled(true);
		label_forward->setEnabled(true);
		label_backward->setEnabled(true);
		label_left->setEnabled(true);
		label_right->setEnabled(true);
		label_speed->setEnabled(true);
	}

	if (!status)
		return;

	uint pPed = proc->read<uint>((void*)pLocalPlayer);
	if (pPed == 0){
		status = false;
		return;
	}

	uint ped_status = proc->read<uint>((void*)(pPed + 0x530));
	if (ped_status == 54 || ped_status == 55 || ped_status == 63){
		status = false;
		return;
	}

	uint local = pPed;
	if (ped_status == 50){
		local = proc->read<uint>((void*)(pPed + 0x58C));
	}

	uint pMatrix = proc->read<uint>((void*)(local + offsetof(CPlaceable, pMatrix)));
	if (pMatrix == 0){
		status = false;
		return;
	}

	RwMatrix matrix = proc->read<RwMatrix>((void*)pMatrix);

	if (rotate->isChecked()){
		float angle = proc->read<float>((void*)pCamRotation) + float(M_PI) / 2.0f;
		matrix.right.fX = cosf(-angle);
		matrix.right.fY = -sinf(-angle);
		matrix.up.fX = sinf(-angle);
		matrix.up.fY = cosf(-angle);
		if (ped_status != 50){
			proc->write<float>((void*)(pPed + 0x558), angle);
			proc->write<float>((void*)(pPed + 0x55C), angle);
		}
	}

	float speed = this->speed->value() / 5.0f;

	if (key[key_up])
		matrix.pos = matrix.getOffset({0.0, 0.0, speed});
	if (key[key_down])
		matrix.pos = matrix.getOffset({0.0, 0.0, -speed});
	if (key[key_forward])
		matrix.pos = matrix.getOffset({0.0, speed, 0.0});
	if (key[key_backward])
		matrix.pos = matrix.getOffset({0.0, -speed, 0.0});
	if (key[key_left])
		matrix.pos = matrix.getOffset({-speed, 0.0, 0.0});
	if (key[key_right])
		matrix.pos = matrix.getOffset({speed, 0.0, 0.0});

	proc->write<RwMatrix>((void*)pMatrix, matrix);
}

void AirBreak::on_toggle_textChanged(const QString &arg1)
{
	key_toggle = getKey(arg1);
	if (!key_toggle)
		valid_toggle->setChecked(false);
	else valid_toggle->setChecked(true);
}

void AirBreak::on_up_textChanged(const QString &arg1)
{
	key_up = getKey(arg1);
	if (!key_up)
		valid_up->setChecked(false);
	else valid_up->setChecked(true);
}

void AirBreak::on_down_textChanged(const QString &arg1)
{
	key_down = getKey(arg1);
	if (!key_down)
		valid_down->setChecked(false);
	else valid_down->setChecked(true);
}

void AirBreak::on_forward_textChanged(const QString &arg1)
{
	key_forward = getKey(arg1);
	if (!key_forward)
		valid_forward->setChecked(false);
	else valid_forward->setChecked(true);
}

void AirBreak::on_backward_textChanged(const QString &arg1)
{
	key_backward = getKey(arg1);
	if (!key_backward)
		valid_backward->setChecked(false);
	else valid_backward->setChecked(true);
}

void AirBreak::on_left_textChanged(const QString &arg1)
{
	key_left = getKey(arg1);
	if (!key_left)
		valid_left->setChecked(false);
	else valid_left->setChecked(true);
}

void AirBreak::on_right_textChanged(const QString &arg1)
{
	key_right = getKey(arg1);
	if (!key_right)
		valid_right->setChecked(false);
	else valid_right->setChecked(true);
}

RwV3D RwMatrix::getOffset(RwV3D offset)
{
	RwV3D offsetVector = pos;
	offsetVector.fX += (offset.fX * right.fX) + (offset.fY * up.fX) + (offset.fZ * at.fX);
	offsetVector.fY += (offset.fX * right.fY) + (offset.fY * up.fY) + (offset.fZ * at.fY);
	offsetVector.fZ += (offset.fX * right.fZ) + (offset.fY * up.fZ) + (offset.fZ * at.fZ);
	return offsetVector;
}

void AirBreak::on_actionDetach_triggered()
{
	status = false;
	proc->setProcess("\\/");
}
