#ifndef OTHERPROCESS_H
#define OTHERPROCESS_H

#include <windows.h>
#include <process.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <QString>
#include <QMap>

class OtherProcess
{
	int pid = 0;
	HANDLE proc = nullptr;
	QString procName;
	QMap<QString, DWORD> modules;
public:
	OtherProcess(QString procName);
	OtherProcess(){}
	~OtherProcess();
	bool hasSuccess() {return (pid && proc);}
	void setProcess(QString procName);

	DWORD operator[](QString modName){
		return modules[modName];
	}

	void *alloc(size_t size);
	void free(void *address, size_t size);
	void protect(void *address, size_t size, DWORD newProtect, DWORD *oldProtect);

	void memset(void *address, char character, size_t size);
	void memcpy(void *address, char *buffer, size_t size);

	template<typename T>
	void write(void *address, T value){
		DWORD dwProtect;
		protect(address, sizeof(T), PAGE_EXECUTE_READWRITE, &dwProtect);
		WriteProcessMemory( proc, address, &value, sizeof(T), NULL );
		protect(address, sizeof(T), dwProtect, NULL);
	}
	template<typename T>
	T read(void *address){
		DWORD dwProtect;
		T value;
		protect(address, sizeof(T), PAGE_EXECUTE_READWRITE, &dwProtect);
		ReadProcessMemory( proc, address, &value, sizeof(T), NULL );
		protect(address, sizeof(T), dwProtect, NULL);
		return value;
	}

private:
	DWORD loadModules();
	int getPid();
};

#endif // OTHERPROCESS_H
