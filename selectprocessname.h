#ifndef SELECTPROCESSNAME_H
#define SELECTPROCESSNAME_H

#include "ui_selectprocessname.h"

class SelectProcessName : public QDialog, private Ui::SelectProcessName
{
	Q_OBJECT

public:
	explicit SelectProcessName(QWidget *parent = 0);

protected:
	void changeEvent(QEvent *e);

signals:
	void onAccepted(QString procesName);

private slots:
	void on_buttonBox_accepted();
};

#endif // SELECTPROCESSNAME_H
